
- Fixed 3rd and 4th Gen O2 Recyclers not counting as oxygen generators for the hint
- Fixed name of 3rd Gen O2 Recycler (was called 02)
- Fixed Fridge v2.0 not counting as a food preparation item for the hint
- Restored massive list of original/Double Fine easter egg names to name list
- Reordered some of the build object menus / fixed some long standing bugs that caused the wrong order to display
- Disabled cuffed prisoners with a brig assigned from randomly turning hostile outside of the brig (cheat/bugfix to get them to the brig)
- Raiders that become citizens now get automatically unassigned from the brig
