local Gui = require('UI.Gui')
local DFUtil = require("DFCommon.Util")
local DFInput = require('DFCommon.Input')
local DFFile = require('DFCommon.File')
local UIElement = require('UI.UIElement')
local Renderer = require('Renderer')
local GameRules = require('GameRules')
local GameScreen = require('GameScreen')

local LoadDirectory = require('UI.LoadDirectory')


local sUILayoutFileName = 'UILayouts/LoadBaseLayout'


local m = {}


function cleanPathString(str)
	local finalString = str:gsub("%/", "\\")
	return finalString
end

function m.create()
	local Ob = DFUtil.createSubclass(UIElement.create())
	local CurrentDir = cleanPathString(tostring(MOAIEnvironment.documentDirectory .. "/Saves"))
    local fileName = "SpacebaseDF9AutoSave.sav" 
	local fileListIndex = 0
	Ob.bDoRolloverCheck = true
	
	--MOAIFileSystem.affirmPath(CurrentDir)

	function Ob:init()
	
		Ob.Parent.init(self)
		
        self:setRenderLayer("UIOverlay")
		
		self:processUIInfo(sUILayoutFileName)
		
		self.uiBG = self:getTemplateElement('BackgroundTop')
		self.uiBG = self:getTemplateElement('BackgroundLeft')
		self.uiBG = self:getTemplateElement('BackgroundRight')
		self.uiBG = self:getTemplateElement('BackgroundBottom')
		
		
		self.logo = self:getTemplateElement('Logo')
		
		
		--self.uiBG = self:getTemplateElement('Background')
		
		self:_updateBackground()
	
		self.rCurrentPathLabel = self:getTemplateElement('CurrentPathLabel')
		self.rCurrentPathLabel:setString(cleanPathString(CurrentDir))
		self.rCurrentPathLabel.sText = cleanPathString(CurrentDir)
		self.rCurrentPathLabelEditBG = self:getTemplateElement('CurrentPathLabelEditBG')
		self.rCurrentPathEditButton = self:getTemplateElement('CurrentPathEditButton')
		self.rCurrentPathEditButton:addPressedCallback(self.onCurrentPathEditButtonPressed, self)

		
		self.rCurrentFileDirectoryList = self:getTemplateElement('CurrentFileDirectoryList')
		self.rCurrentFileDirectoryList:setRenderLayer('UIScrollLayerLeft')
		self.rCurrentFileDirectoryList:setScissorLayer('UIScrollLayerLeft')
		
		
		self.rAlertBox = self:getTemplateElement('AlertBox')
		self.rAlertBox.sText = "" 
		self.rAlertBox:setString("")
			
		self.rFilenameBox = self:getTemplateElement('FilenameBox')
		self.rFilenameBox.sText = ""
		self.rFilenameBox:setString("")

		self.rButtonLoad = self:getTemplateElement('LoadButton')
        self.rButtonLoad:addPressedCallback(self.loadFile, self)
		self.rButtonLoadHotkey = self:getTemplateElement('LoadHotkey')
		
		self.rButtonLoadCancel = self:getTemplateElement('CancelButton')
        self.rButtonLoadCancel:addPressedCallback(self.loadCancel, self)
		self.rButtonLoadCancelHotkey = self:getTemplateElement('CancelHotkey')
		
	end


	function Ob:generateDirList()
		local tFiles = MOAIFileSystem.listFiles(CurrentDir)
		local nFileNumEntries = table.getn(tFiles)
				
		if nFileNumEntries > 0 then
			for k, v in ipairs(tFiles) do
				self:addFileToListEntry(k, v)
			end
		end
		fileListIndex = nFileNumEntries
		self.rCurrentFileDirectoryList:refresh()

	end
	
	function Ob:addFileToListEntry(id, name)
		local rNewEntry = self:addFileToList(id, 0)
		rNewEntry:setName(id, name, self.onFileClickCallback)
	end
	
	
	function Ob:addFileToList(index, x)
		local rNewEntry = LoadDirectory.new()
		local w,h = rNewEntry:getDims()
		local nYLoc = h * index + 50
		rNewEntry:setLoc(x, nYLoc)
		self:_calcDimsFromElements()
		self.rCurrentFileDirectoryList:addScrollingItem(rNewEntry)
		return rNewEntry
	end
	
	function Ob:onFileClickCallback(id, name)
		--Activate Load Button
		
		Ob.rFilenameBox:setString(name)
		Ob.rFilenameBox.sText = name
	end
	
	
	function Ob:loadFile()
		--get file name
		if self.rFilenameBox.sText == "" then
			self:setElementHidden(self.rAlertBox, false) 
			self.rAlertBox:setString("File Not Selected")
			self.rAlertBox.sText = "File Not Selected"
			self.rFilenameBox.sText = ""
			self.rFilenameBox:setString("")
			return false
		end
		
		if string.sub(self.rFilenameBox.sText, -4) ~= ".sav" then
			self:setElementHidden(self.rAlertBox, false) 
			self.rAlertBox:setString("File Not .sav")
			self.rAlertBox.sText = "File Not .sav"
			self.rFilenameBox.sText = ""
			self.rFilenameBox:setString("")
			return false
		end
		
		--get path
		CurrentDir = self.rCurrentPathLabel.sText
		filePath = cleanPathString(CurrentDir)
		
		if not MOAIFileSystem.checkPathExists(filePath) then
			self:setElementHidden(self.rAlertBox, false)
			self.rAlertBox:setString("Directory Not Found")
			self.rAlertBox.sText = "Directory Not Found"
			self.rFilenameBox.sText = ""
			self.rFilenameBox:setString("")
			return false
		end
		
		if string.sub(CurrentDir, -1) == "\\"  then
			fullPath = CurrentDir .. self.rFilenameBox.sText
		else
			fullPath = CurrentDir .. "\\" .. self.rFilenameBox.sText
		end
		
		if not MOAIFileSystem.checkFileExists(fullPath) then
			self:setElementHidden(self.rAlertBox, false)
			self.rAlertBox:setString("Directory Not Found")
			self.rAlertBox.sText = "Directory Not Found"
			self.rFilenameBox.sText = ""
			self.rFilenameBox:setString("")
			return false
		end
		-- load game
		GameRules.loadGame(fullPath, true)
		g_filename = fullPath
		g_bGameLoaded = true
		GameRules.startLoop()
		
	end
	
	function Ob:removeFileEntry(nIndex)
		self.rCurrentFileDirectoryList:removeScrollingItem(self.rCurrentFileDirectoryList.tItems[nIndex])
	end
	
	function Ob:resetFileList()
		for iLoop = fileListIndex, 1, -1 do
			self.rCurrentFileDirectoryList:removeScrollingItem(self.rCurrentFileDirectoryList.tItems[iLoop])
		end
	end
	

	function Ob:onCurrentPathEditButtonPressed(rButton, eventType)
		if eventType == DFInput.TOUCH_UP and not GameScreen.inTextEntry() then
			g_inLoadOrSave = true
			GameScreen.beginTextEntry(self.rCurrentPathLabel, self, self.confirmTextEntry, self.cancelTextEntry)
			self.rCurrentPathEditButton:setSelected(true)
			self.rCurrentFileDirectoryList:refresh()
		end
	end
	
	

	function Ob:confirmTextEntry(text)
		print("In ConfirmTextEntry")
		if text then
			cleanString = cleanPathString(text)
			CurrentDir = cleanString
			
			if MOAIFileSystem.checkPathExists(cleanString) then 
				--self:setElementHidden(self.rAlertBox, true) 
				self:resetFileList()
				self.rCurrentPathLabel:setString(cleanString)
				self.rCurrentPathLabel.sText = cleanString
				self.rCurrentPathEditButton:setSelected(false)
				self:generateDirList()
				self.rCurrentFileDirectoryList:reset()
				
			else
				--self.rAlertBox:setString("Directory Not Found")
				--self.rAlertBox.sText = "Directory Not Found"
				--self:setElementHidden(self.rAlertBox, false) 
				self:resetFileList()
				self.rCurrentFileDirectoryList:refresh()
			end
		end
	end
	
	function Ob:cancelTextEntry(text)
		print("In cancelTextEntry")
		self.rCurrentPathEditButton:setSelected(false)
		g_inLoadOrSave = false
	end
	

	function Ob:loadCancel()
		self:resetFileList()
		CurrentDir = cleanPathString(tostring(MOAIEnvironment.documentDirectory .. "/Saves"))
		self.rCurrentPathLabel:setString(CurrentDir)
		self.rCurrentPathLabel.sText = CurrentDir
		self.rFilenameBox.sText = ""
		self.rFilenameBox:setString("")
		self.rAlertBox.sText = ""
		self.rAlertBox:setString("")
		g_GuiManager.showStartMenu(true)
	end
	
    function Ob:_updateBackground()
        --self.uiBG:setScl(Renderer.getViewport().sizeX*2,Renderer.getViewport().sizeY*2)
        --self.uiBG:setLoc(-Renderer.getViewport().sizeX*.5,Renderer.getViewport().sizeY*.5)
	end
	
	function Ob:show(basePri)
		Ob.Parent.show(self, basePri)
		
	    g_GuiManager.statusBar:hide()
		g_GuiManager.tutorialText:hide()
        g_GuiManager.hintPane:hide()
        g_GuiManager.alertPane:hide()
		g_GuiManager.newSideBar:hide()
		
        GameRules.timePause()
        self:onResize()
		
		self:generateDirList()
		self.rCurrentFileDirectoryList:reset()
		return nPri
	end
	

    function Ob:onTick(dt)
        Ob.Parent.onTick(self, dt)
    end

    function Ob:playWarbleEffect(bFullscreen)
        if bFullscreen then
            local uiX,uiY,uiW,uiH = Renderer.getUIViewportRect()            
            g_GuiManager.createEffectMaskBox(0, 0, uiW, uiH, 0.3)
        else
            g_GuiManager.createEffectMaskBox(0, 0, 500, 1444, 0.3, 0.3)
        end
    end    
	
    function Ob:onFinger(touch, x, y, props)
		Ob.Parent.onFinger(self, touch, x, y, props)
		if self.rCurrentFileDirectoryList:onFinger(touch, x, y, props) then
            bHandled = true
        end
        if self.rCurrentFileDirectoryList:isInsideScrollPane(x, y) then
                   
        end
    end
	
    function Ob:inside(wx, wy)
		self.rCurrentFileDirectoryList:inside(wx, wy)
		return Ob.Parent.inside(self, wx, wy)
    end
	
	function Ob:onKeyboard(key, bDown)
        -- capture all keyboard input
        if bDown and key == 27 then -- esc
			self:loadCancel()
        end
		
		if bDown and key == 67 then -- C
			self:loadCancel()
		end
		
		if bDown and key == 99 then -- c
			self:loadCancel()
		end
		
		if bDown and key == 76 then -- L
			self:loadFile()
		end
		
		if bDown and key == 108 then -- l
			self:loadFile()
		end
		
        return true
    end
	
	function Ob:onResize()
        Ob.Parent.onResize(self)
        self:_updateBackground()
    end
	
	function Ob:onBackButtonPressed(rButton, eventType)
        if eventType == DFInput.TOUCH_UP then
			menuManager.closeMenu()
			SoundManager.playSfx('degauss')
        end
    end
    return Ob
end

function m.new(...)
    local Ob = m.create()
    Ob:init(...)

    return Ob
end

return m
