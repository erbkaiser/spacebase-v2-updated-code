local Gui = require('UI.Gui')
local DFUtil = require("DFCommon.Util")
local DFInput = require('DFCommon.Input')
local UIElement = require('UI.UIElement')
local Renderer = require('Renderer')
local SoundManager = require('SoundManager')

local m = {}


local sUILayoutFileName = 'UILayouts/LoadDirectoryLayout'


function cleanPathString(str)
	finalString = str:gsub("%/", "\\")
	return finalString
end


function m.create()
	local Ob = DFUtil.createSubclass(UIElement.create())
	local id
	local name
	
	
	function Ob:init()

		self:setRenderLayer('UIScrollLayerLeft')
		
        Ob.Parent.init(self)
		self:processUIInfo(sUILayoutFileName)
		
		self.rNameLabel = self:getTemplateElement('NameLabel')
		self.rNameButton = self:getTemplateElement('NameButton')
		self.rNameButton:addPressedCallback(self.onButtonClicked, self)
		
		
		self:_calcDimsFromElements()
		
	end
	
	function Ob:setName(_id, _name, callback)
		id = _id
		name = _name
		self.rNameLabel:setString(name)
		self.callback = callback
	end
	
	function Ob:onButtonClicked(rButton, eventType)
		if eventType == DFInput.TOUCH_UP then
			self:callback(id, name)
			SoundManager.playSfx('accept')
		end
	end
	
	return Ob
end

function m.new(...)
    local Ob = m.create()
    Ob:init(...)

    return Ob
end

return m