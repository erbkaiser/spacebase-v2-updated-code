local Gui = require('UI.Gui')

local AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.95 }
local BRIGHT_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 1 }
local SELECTION_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.01 }

local nGuiYval = g_GuiManager.getUIViewportSizeY()


return
    {
        posInfo =
            {
                alignX = 'left',
                alignY = 'top',
                offsetX = 0,
                offsetY = 0,
            },
        tElements =
            {
				{
					key = 'BackgroundTop',
					type = 'onePixel',
					pos = { 0, 0 },
					scale = { 2568, 462 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundLeft',
					type = 'onePixel',
					pos = { 0, 0 },
					scale = { 205, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundRight',
					type = 'onePixel',
					pos = { 1433, 0 },
					scale = { 2568, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundBottom',
					type = 'onePixel',
					pos = { 0, -(nGuiYval - 207) },
					scale = { 2568, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'Logo',
					type = 'uiTexture',
					textureName = 'logo',
					sSpritesheetPath = 'UI/StartMenu',
					--pos = { 57, 50 },
					pos = { 64, 50 },
					scale = { 1.5, 1.5 },
					color = Gui.WHITE,
					hidden = false,
				},
                {
                    key = 'HeaderText',
                    type = 'textBox',
                    pos = { 1005, -380 },
                    text = "SAVE BASE",
                    style = 'orbitronWhite',
                    --rect = { 0, 100,'g_GuiManager.getUIViewportSizeX() - 50', 0 },
                    rect = { 0, 70, 800, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
                    vAlign = MOAITextBox.CENTER_JUSTIFY,
                    color = Gui.White,
                },				
				
				
				-- main border
				{
					key = 'UpperLeftCorner',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 200, -450 },
					scale = { 2, 2 },
					color = Gui.AMBER,
				},
				{
					key = 'UpperRightCorner1',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 1846, -450 }, --(g_GuiManager.getUIViewportSizeX() - 200)
					scale = { -2, 2 },
					color = Gui.AMBER,
				},
				{
					key = 'UpperRightCorner2',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 1846, -(nGuiYval - 212) }, --(g_GuiManager.getUIViewportSizeX() - 200)
					scale = { -2, 2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomLeftCorner',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					--pos = { 200, -1061 },
					pos = { 200, -(nGuiYval - 106) },
					scale = { 2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomRightCorner1',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 1846, -(nGuiYval - 106) }, --(g_GuiManager.getUIViewportSizeX() - 200)
					--pos = { 1439, -(nGuiYval - 106) },
					scale = { -2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomRightCorner2',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					--pos = { 1846, -1061 }, --(g_GuiManager.getUIViewportSizeX() - 200)
					pos = { 1846, -656 },
					scale = { -2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'TopBorder',
					type = 'onePixel',
					pos = { 206, -450 },
					scale = { 1635, 6 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomBorder',
					type = 'onePixel',
					--pos = { 206, -1055 },
					pos = { 206, -(nGuiYval - 112) },
					scale = { 1635, 6 },
					--scale = { 1230, 6 },
					color = Gui.AMBER,
				},
				{
					key = 'LeftBorder',
					type = 'onePixel',
					pos = { 200, -457 },
					scale = { 6, (nGuiYval - 570) },
					color = Gui.AMBER,
				},
				{
					key = 'RightBorder',
					type = 'onePixel',
					pos = { 1840, -457 }, -- (g_GuiManager.getUIViewportSizeX() - 206)
					--scale = { 6, 290 },
					scale = { 6, 193 },
					color = Gui.AMBER,
				},
				{
					key = 'RightBorder2',
					type = 'onePixel',
					pos = { 1840, -(nGuiYval - 208) }, -- (g_GuiManager.getUIViewportSizeX() - 206)
					--scale = { 6, 290 },
					scale = { 6, 94 },
					color = Gui.AMBER,
				},
				-- inner borders
				{
					key = 'InnerTopBorder',
					type = 'onePixel',
					pos = { 1439, -550 },
					--scale = { 9, 955 },
					scale = { 402, 6 }, --818
					color = Gui.AMBER,
				},
				{
					key = 'InnerMiddleBorder', --Dirlist\FileName bottom border
					type = 'onePixel',
					pos = { 206, -(nGuiYval - 212) },
					--scale = { 1230, 6 }, --1635
					scale = { 1635, 6 },
					color = Gui.AMBER,
				},
				{
					key = 'InnerButtonBorder', -- Load / Cancel divider
					type = 'onePixel',
					--pos = { 205, -955 },
					pos = { 1439, -650 },
					scale = { 402, 6 }, --1635
					--scale = { 1635, 6 }, 
					color = Gui.AMBER,
				},
				{
					key = 'InnerButtonDividerBorder', -- Load / Cancel / Directory Border
					type = 'onePixel',
					pos = { 1433, -450 },
					--pos = { 1433, -955 },
					--scale = { 6, 510 },
					scale = { 6, (nGuiYval - 660)}, --560
					color = Gui.AMBER,
				},
				
				--load and cancel buttons
				{
					key = 'SaveButton',
					type = 'onePixelButton',
					pos = { 1441, -459 },
					scale = { 396, 86 },
					--pos = { 1034, -965 },
					--scale = { 396, 86 },
					color = Gui.BROWN,
					onPressed =
					{
						{
							key = 'SaveButton',
							color = BRIGHT_AMBER,
						},
					},
					onReleased =
					{
						{
							key = 'SaveButton',
							color = AMBER,
						},
					},
					onHoverOn =
					{
						{
							key = 'SaveButton',
							color = AMBER,
						},
						{
							key = 'SaveLabel',
							color = { 0, 0, 0 },
						},
						{
							key = 'SaveHotkey',
							color = { 0, 0, 0 },
						},
						{
							playSfx = 'hilight',
						},
					},
					onHoverOff =
					{
						{
							key = 'SaveButton',
							color = Gui.BROWN,
						},
						{
							key = 'SaveLabel',
							color = Gui.AMBER,
						},
						{
							key = 'SaveHotkey',
							color = Gui.AMBER,
						},
					},
				},
				{
					key = 'SaveLabel',
					type = 'textBox',
					pos = { 1441, -459 },
					--pos = { 1034, -965 },
					text = 'SAVE GAME',
					style = 'dosismedium44',
					rect = { 0, 86, 396, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
					vAlign = MOAITextBox.CENTER_JUSTIFY,
					color = Gui.AMBER,
				},
				{
					key = 'SaveHotkey',
					type = 'textBox',
					pos = { 1788, -507 },
					--pos = { 1400, -1015 },
					text = 'S',
					style = 'dosissemibold30',
					rect = { 0, 100, 100, 0 },
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.AMBER,
				},
				{
					key = 'CancelButton',
					type = 'onePixelButton',
					--pos = { 1441, -965 },
					pos = { 1441, -560 },
					scale = { 396, 86 },
					color = Gui.BROWN,
					onPressed =
					{
						{
							key = 'CancelButton',
							color = BRIGHT_AMBER,
						},
					},
					onReleased =
					{
						{
							key = 'CancelButton',
							color = AMBER,
						},
					},
					onHoverOn =
					{
						{
							key = 'CancelButton',
							color = AMBER,
						},
						{
							key = 'CancelLabel',
							color = { 0, 0, 0 },
						},
						{
							key = 'CancelHotkey',
							color = { 0, 0, 0 },
						},
						{
							playSfx = 'hilight',
						},
					},
					onHoverOff =
					{
						{
							key = 'CancelButton',
							color = Gui.BROWN,
						},
						{
							key = 'CancelLabel',
							color = Gui.AMBER,
						},
						{
							key = 'CancelHotkey',
							color = Gui.AMBER,
						},
					},
				},
				{
					key = 'CancelLabel',
					type = 'textBox',
					--pos = { 1442, -965 },
					pos = { 1442, -560 },
					text = 'CANCEL',
					style = 'dosismedium44',
					rect = { 0, 86, 396, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
					vAlign = MOAITextBox.CENTER_JUSTIFY,
					color = Gui.AMBER,
				},
				{
					key = 'CancelHotkey',
					type = 'textBox',
					--pos = { 1788, -1015 },
					pos = { 1788, -609 },
					text = 'ESC',
					style = 'dosissemibold30',
					rect = { 0, 100, 100, 0 },
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.AMBER,
				},
				-- directory line edit
				{
					key = 'CurrentPathLabelEditBG',
					type = 'onePixelButton',
					pos = { 216, -(nGuiYval - 195) },
					scale = { 1840, 70 }, --800
					--scale = { 1210, 70 }, --800
					color = { 0, 0, 0, 0 },
				},
				{
					key = 'CurrentPathLabel',
					type = 'textBox',
					pos = { 216, -(nGuiYval - 195) },
					text = 'CurrentPathLabel text',
					style = 'dosismedium44',
					--rect = { 0, 20, 300, 0 },
					rect = {0, 70, 1840, 0},
					--rect = {0, 70, 1210, 0}, -- 0,70,800,0
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.WHITE,
				},
				{
					key = 'CurrentPathEditButton',
					type = 'onePixelButton',
					pos = { 216, -(nGuiYval - 195)},
					scale = { 1840, 70 }, --800
					--scale = { 1210, 70 }, --800
					color = { 0, 0, 0, 0 },
					hidden = false,
					clickWhileHidden = true,
					
					onHoverOn =
					{
						{ key = 'CurrentPathLabelTexture', hidden = false, },
					},
					
					onHoverOff =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
					onSelectedOn =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
					onSelectedOff =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
				},
				{
					key = 'CurrentPathLabelTexture',
					type = 'uiTexture',
					textureName = 'ui_inspector_buttonEdit',
					sSpritesheetPath = 'UI/Inspector',
					pos = { 1840, -(nGuiYval - 195) }, --1430
					color = Gui.AMBER,
					hidden = true,
				},
				--scrollpane
				{
					key = 'CurrentFileDirectoryList',
					type = 'scrollPane',
					pos = { 216, -459 },
					--rect = { 0, 0, 1600, 274 },
					rect = { 0, 0, 1200, (nGuiYval - 700) },
				},
				{
					key = 'ScrollBackground',
					type = 'onePixel',
					pos = { 205, -459 },
					scale = { 1230, (nGuiYval - 470) },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
					layerOverride = 'UI',
				},
				{
					key = 'AlertBox',
					type = 'textBox',
					--pos = { 216, -560 },
					pos = { 216, -(nGuiYval - 105) },
					text = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
					style = 'dosismedium44',
					--rect = {0, 100, 400, 0}, -- 0,70,800,0
					rect = {0, 100, 3000, 0},
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.WHITE,
					hidden = false,
				},
				
			}
}