local Gui = require('UI.Gui')

--[[
local AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.95 }
local BRIGHT_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 1 }
local SELECTION_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.01 }
]]--


return
{
    posInfo =
    {
        alignX = 'left',
        alignY = 'top',
		offsetX = 0,
        offsetY = 0,
    },
    tElements =
    {
		{
            key = 'NameButton',
            type = 'onePixelButton',
            pos = { 0, 0 },
            scale = { 1180, 50 },
			color = { 0, 0, 0 },
			onHoverOn =
            {
                {
                    key = 'NameButton',
					color = Gui.AMBER,
                },
                {
                    key = 'NameLabel',
					color = Gui.BLACK,
                },
            },
            onHoverOff =
            {
                {
                    key = 'NameButton',
					color = Gui.BLACK,
                },
                {
                    key = 'NameLabel',
					color = Gui.AMBER,
                },
            },
			onSelectedOn =
			{
				{ key = 'NameButton', color = Gui.AMBER, },
				{ key = 'NameLabel', color = Gui.WHITE },
                { playSfx = 'hilight', },
			},
            onSelectedOff =
            {
                { key = 'NameButton', color = Gui.BLACK, },
				{ key = 'NameLabel', color = Gui.AMBER },
            },
        },
		{
            key = 'NameLabel',
            type = 'textBox',
            pos = { 0, 0 },
            text = "...",
            style = 'dosissemibold35',
			rect = { 0, 50, 1180, 0 },
			hAlign = MOAITextBox.LEFT_JUSTIFY,
            vAlign = MOAITextBox.LEFT_JUSTIFY,
            color = Gui.AMBER,
        },
		
	},
}