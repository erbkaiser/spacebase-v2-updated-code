local Gui = require('UI.Gui')

local AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.95 }
local BRIGHT_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 1 }
local SELECTION_AMBER = { Gui.AMBER[1], Gui.AMBER[2], Gui.AMBER[3], 0.01 }

local nGuiYval = g_GuiManager.getUIViewportSizeY()
-- Have to use the actually value if you use getUIViewportSixe()

-- Save button is about 300 pixels wide for scale

return
    {
        posInfo =
            {
                alignX = 'left',
                alignY = 'top',
                offsetX = 0,
                offsetY = 0,
            },
        tElements =
            {
				{
					key = 'BackgroundTop',
					type = 'onePixel',
					pos = { 0, 0 },
					scale = { 2568, 565 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundLeft',
					type = 'onePixel',
					pos = { 0, 0 },
					scale = { 205, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundRight',
					type = 'onePixel',
					pos = { 1433, 0 },
					scale = { 2568, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'BackgroundBottom',
					type = 'onePixel',
					pos = { 0, -(nGuiYval - 105) },
					scale = { 2568, 1444 },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
				},
				{
					key = 'Logo',
					type = 'uiTexture',
					textureName = 'logo',
					sSpritesheetPath = 'UI/StartMenu',
					--pos = { 57, 50 },
					pos = { 64, 50 },
					scale = { 1.5, 1.5 },
					color = Gui.WHITE,
					hidden = false,
				},
                {
                    key = 'HeaderText',
                    type = 'textBox',
                    pos = { 1005, -380 },
                    text = "LOAD BASE",
                    style = 'orbitronWhite',
                    --rect = { 0, 100,'g_GuiManager.getUIViewportSizeX() - 50', 0 },
                    rect = { 0, 70, 800, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
                    vAlign = MOAITextBox.CENTER_JUSTIFY,
                    color = Gui.White,
                },	
				
				-- main border
				{
					key = 'UpperLeftCorner',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 200, -450 },
					scale = { 2, 2 },
					color = Gui.AMBER,
				},
				{
					key = 'UpperRightCorner',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					pos = { 1846, -450 }, --(g_GuiManager.getUIViewportSizeX() - 200)
					scale = { -2, 2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomLeftCorner',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					--pos = { 200, -1061 },
					pos = { 200, -(nGuiYval - 106) },
					scale = { 2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomRightCorner1',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					--pos = { 1846, -1061 }, --(g_GuiManager.getUIViewportSizeX() - 200)
					pos = { 1439, -(nGuiYval - 106) },
					scale = { -2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomRightCorner2',
					type = 'uiTexture',
					textureName = 'ui_boxCorner_3px',
					sSpritesheetPath = 'UI/Shared',
					--pos = { 1846, -1061 }, --(g_GuiManager.getUIViewportSizeX() - 200)
					pos = { 1846, -856 },
					scale = { -2, -2 },
					color = Gui.AMBER,
				},
				{
					key = 'TopBorder',
					type = 'onePixel',
					pos = { 206, -450 },
					scale = { 1635, 6 },
					color = Gui.AMBER,
				},
				{
					key = 'BottomBorder',
					type = 'onePixel',
					--pos = { 206, -1055 },
					pos = { 206, -(nGuiYval - 112) },
					--scale = { 1635, 6 },
					scale = { 1230, 6 },
					color = Gui.AMBER,
				},
				{
					key = 'LeftBorder',
					type = 'onePixel',
					pos = { 200, -457 },
					scale = { 6, (nGuiYval - 570) },
					color = Gui.AMBER,
				},
				{
					key = 'RightBorder',
					type = 'onePixel',
					pos = { 1840, -457 }, -- (g_GuiManager.getUIViewportSizeX() - 206)
					--scale = { 6, 290 },
					scale = { 6, 393 },
					color = Gui.AMBER,
				},
				-- inner borders
				{
					key = 'InnerTopBorder',
					type = 'onePixel',
					pos = { 206, -550 },
					--scale = { 9, 955 },
					scale = { 1635, 6 }, --818
					color = Gui.AMBER,
				},
				{
					key = 'InnerMiddleBorder', --Cancel\FileName bottom border
					type = 'onePixel',
					--pos = { 1024, -956 },
					--scale = { 6, 100 },
					pos = { 1439, -750 },
					scale = { 402, 6 }, --1635
					color = Gui.AMBER,
				},
				{
					key = 'FilenameBottomBorder', --Filename bottom border
					type = 'onePixel',
					--pos = { 1024, -956 },
					--scale = { 6, 100 },
					pos = { 1439, -850 },
					scale = { 402, 6 }, --1635
					color = Gui.AMBER,
				},
				{
					key = 'InnerButtonBorder', -- Load / Cancel divider
					type = 'onePixel',
					--pos = { 205, -955 },
					pos = { 1439, -650 },
					scale = { 402, 6 }, --1635
					--scale = { 1635, 6 }, 
					color = Gui.AMBER,
				},
				{
					key = 'InnerButtonDividerBorder', -- Load / Cancel / File Border
					type = 'onePixel',
					pos = { 1433, -550 },
					--pos = { 1433, -955 },
					--scale = { 6, 510 },
					scale = { 6, (nGuiYval - 660) }, --1635
					color = Gui.AMBER,
				},

				--load and cancel buttons
				{
					key = 'LoadButton',
					type = 'onePixelButton',
					pos = { 1441, -560 },
					scale = { 396, 86 },
					--pos = { 1034, -965 },
					--scale = { 396, 86 },
					color = Gui.BROWN,
					onPressed =
					{
						{
							key = 'LoadButton',
							color = BRIGHT_AMBER,
						},
					},
					onReleased =
					{
						{
							key = 'LoadButton',
							color = AMBER,
						},
					},
					onHoverOn =
					{
						{
							key = 'LoadButton',
							color = AMBER,
						},
						{
							key = 'LoadLabel',
							color = { 0, 0, 0 },
						},
						{
							key = 'LoadHotkey',
							color = { 0, 0, 0 },
						},
						{
							playSfx = 'hilight',
						},
					},
					onHoverOff =
					{
						{
							key = 'LoadButton',
							color = Gui.BROWN,
						},
						{
							key = 'LoadLabel',
							color = Gui.AMBER,
						},
						{
							key = 'LoadHotkey',
							color = Gui.AMBER,
						},
					},
				},
				{
					key = 'LoadLabel',
					type = 'textBox',
					pos = { 1441, -560 },
					--pos = { 1034, -965 },
					text = 'LOAD GAME',
					style = 'dosismedium44',
					rect = { 0, 86, 396, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
					vAlign = MOAITextBox.CENTER_JUSTIFY,
					color = Gui.AMBER,
				},
				{
					key = 'LoadHotkey',
					type = 'textBox',
					pos = { 1788, -607 },
					--pos = { 1400, -1015 },
					text = 'L',
					style = 'dosissemibold30',
					rect = { 0, 100, 100, 0 },
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.AMBER,
				},
				
				
				{
					key = 'CancelButton',
					type = 'onePixelButton',
					--pos = { 1441, -965 },
					pos = { 1441, -659 },
					scale = { 396, 86 },
					color = Gui.BROWN,
					onPressed =
					{
						{
							key = 'CancelButton',
							color = BRIGHT_AMBER,
						},
					},
					onReleased =
					{
						{
							key = 'CancelButton',
							color = AMBER,
						},
					},
					onHoverOn =
					{
						{
							key = 'CancelButton',
							color = AMBER,
						},
						{
							key = 'CancelLabel',
							color = { 0, 0, 0 },
						},
						{
							key = 'CancelHotkey',
							color = { 0, 0, 0 },
						},
						{
							playSfx = 'hilight',
						},
					},
					onHoverOff =
					{
						{
							key = 'CancelButton',
							color = Gui.BROWN,
						},
						{
							key = 'CancelLabel',
							color = Gui.AMBER,
						},
						{
							key = 'CancelHotkey',
							color = Gui.AMBER,
						},
					},
				},
				{
					key = 'CancelLabel',
					type = 'textBox',
					--pos = { 1442, -965 },
					pos = { 1442, -659 },
					text = 'CANCEL',
					style = 'dosismedium44',
					rect = { 0, 86, 396, 0 },
					hAlign = MOAITextBox.CENTER_JUSTIFY,
					vAlign = MOAITextBox.CENTER_JUSTIFY,
					color = Gui.AMBER,
				},
				{
					key = 'CancelHotkey',
					type = 'textBox',
					--pos = { 1788, -1015 },
					pos = { 1788, -709 },
					text = 'ESC',
					style = 'dosissemibold30',
					rect = { 0, 100, 100, 0 },
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.AMBER,
				},
				
				-- directory line edit
				
				{
					key = 'CurrentPathLabelEditBG',
					type = 'onePixelButton',
					pos = { 216, -473 },
					scale = { 1620, 70 }, --800
					--color = Gui.AMBER,
					--color = Gui.BLACK,
					color = { 0, 0, 0, 0 },
				},
				{
					key = 'CurrentPathLabel',
					type = 'textBox',
					pos = { 216, -475 },
					text = 'CurrentPathLabel text',
					style = 'dosismedium44',
					--rect = { 0, 20, 300, 0 },
					rect = {0, 70, 1625, 0}, -- 0,70,800,0
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.WHITE,
				},
				{
					key = 'CurrentPathEditButton',
					type = 'onePixelButton',
					pos = { 216, -473 },
					scale = { 1625, 70 }, --800
					color = { 1, 0, 0 },
					hidden = true,
					clickWhileHidden = true,
					
					onHoverOn =
					{
						{ key = 'CurrentPathLabelTexture', hidden = false, },
					},
					
					onHoverOff =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
					onSelectedOn =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
					onSelectedOff =
					{
						{ key = 'CurrentPathLabelTexture', hidden = true, },
					},
				},
				{
					key = 'CurrentPathLabelTexture',
					type = 'uiTexture',
					textureName = 'ui_inspector_buttonEdit',
					sSpritesheetPath = 'UI/Inspector',
					pos = { 1840, -473 }, --1016
					color = Gui.AMBER,
					hidden = true,
				},
				{
					key = 'CurrentFileDirectoryList',
					type = 'scrollPane',
					pos = { 216, -565 },
					--rect = { 0, 0, 1600, 274 },
					rect = { 0, 0, 1200, (nGuiYval - 700) },
				},
				{
					key = 'ScrollBackground',
					type = 'onePixel',
					pos = { 205, -565 },
					scale = { 1230, (nGuiYval - 670) },
					--color = { 0.5, 0.5, 0.5, 1 },
					color = { 0, 0, 0, 1 },
					hidden = false,
					layerOverride = 'UI',
				},
				{
					key = 'AlertBox',
					type = 'textBox',
					--pos = { 216, -560 },
					pos = { 216, -(nGuiYval - 105) },
					text = 'Directory Not Found',
					style = 'dosismedium44',
					rect = {0, 100, 400, 0}, -- 0,70,800,0
					hAlign = MOAITextBox.LEFT_JUSTIFY,
					vAlign = MOAITextBox.LEFT_JUSTIFY,
					color = Gui.WHITE,
					hidden = false,
				},
				{
					key = 'FilenameBox',
					type = 'textBox',
					pos = { 1441, -859 },
					text = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
					style = 'dosismedium32',
					rect = {0, 0, 396, 120}, -- 0,0,396,86
					hAlign = MOAITextBox.CENTER_JUSTIFY,
					vAlign = MOAITextBox.CENTER_JUSTIFY,
					color = Gui.WHITE,
					hidden = false,
				},
			}
	}